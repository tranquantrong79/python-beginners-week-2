u, v, x, y, z = 29, 12, 10, 4, 3

#a
print(u/v)

#b
t = (u == v)
print(t)

#c
print(u % x)

#d
t = (x >= y)
print(t)

#e
u += 5
print(u)

#f
u %=z
print(u)

#g
t = (v > x and y < z)
print(t)

#h
print(x**z)

#i
print(x // z)